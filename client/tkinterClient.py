
#TRUC DE JOAN GUI MESSAGE, 


import tkinter as tk
from tkinter import ttk
from client import Client

LARGEFONT =("Verdana", 35)

class tkinterApp(tk.Tk):
    # __init__ function for class tkinterApp 
    def __init__(self, *args, **kwargs): 
         
        # __init__ function for class Tk
        tk.Tk.__init__(self, *args, **kwargs)
         
        # creating a container
        container = tk.Frame(self)  
        container.pack(side = "top", fill = "both", expand = True) 
  
        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)
  
        # initializing frames to an empty array
        self.frames = {}  
  
        # iterating through a tuple consisting
        # of the different page layouts
        for F in (StartPage, Page1, Page2):
  
            frame = F(container, self)
  
            # initializing frame of that object from
            # startpage, page1, page2 respectively with 
            # for loop
            self.frames[F] = frame 
  
            frame.grid(row = 0, column = 0, sticky ="nsew")
  
        self.show_frame(StartPage)
  
    # to display the current frame passed as
    # parameter
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()
  
# first window frame startpage
  
class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # label of frame Layout 2
        label = ttk.Label(self, text ="LOGIN")
        label.grid(row = 0, column = 4, padx = 10, pady = 10)

        #username = ttk.StringVar() 
        labelUsername = ttk.Label(self, text ="Username").grid(row = 1, column = 0, padx = 10, pady = 10)
        username=ttk.Entry(self).grid(row = 1, column = 1, padx = 10, pady = 10, )

        #host = ttk.StringVar() 
        labelHost = ttk.Label(self, text ="Host").grid(row = 2, column = 0, padx = 10, pady = 10)
        host=ttk.Entry(self).grid(row = 2, column = 1, padx = 10, pady = 10, )

        #port = ttk.StringVar() 
        labelPort = ttk.Label(self, text ="Port").grid(row = 3, column = 0, padx = 10, pady = 10)
        port=ttk.Entry(self).grid(row = 3, column = 1, padx = 10, pady = 10, )

        validateButton = ttk.Button(self, text ="JOIN TEXT CHANNEL",
        command = self.connect(username.get(), host.get(), port.get()))

        # putting the button in its place by
        # using grid
        validateButton.grid(row = 4, column = 1, padx = 10, pady = 10)

        # button to show frame 2 with text layout2
        exitButton = ttk.Button(self, text ="EXIT",command = exit)

        # putting the button in its place by
        # using grid
        exitButton.grid(row = 5, column = 1, padx = 10, pady = 10)

    def connect(self, username, host, port):
        print(username)
        client= Client(username, host, port)
        client.listen()
        message= ""
        while message!="QUIT":
            message= input()
            client.send(message)
  
# second window frame page1
class Page1(tk.Frame):
    
    def __init__(self, parent, controller):     
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text ="Page 1", font = LARGEFONT)
        label.grid(row = 0, column = 4, padx = 10, pady = 10)
        # button to show frame 2 with text
        # layout2
        button1 = ttk.Button(self, text ="StartPage", command = lambda : controller.show_frame(StartPage))
     
        # putting the button in its place 
        # by using grid
        button1.grid(row = 1, column = 1, padx = 10, pady = 10)
  
        # button to show frame 2 with text
        # layout2
        button2 = ttk.Button(self, text ="Page 2",
                            command = lambda : controller.show_frame(Page2))

        # putting the button in its place by 
        # using grid
        button2.grid(row = 2, column = 1, padx = 10, pady = 10)

# third window frame page2
class Page2(tk.Frame): 
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text ="Page 2", font = LARGEFONT)
        label.grid(row = 0, column = 4, padx = 10, pady = 10)

        # button to show frame 2 with text
        # layout2
        button1 = ttk.Button(self, text ="Page 1",
                            command = lambda : controller.show_frame(Page1))

        # putting the button in its place by 
        # using grid
        button1.grid(row = 1, column = 1, padx = 10, pady = 10)

        # button to show frame 3 with text
        # layout3
        button2 = ttk.Button(self, text ="Startpage",
                            command = lambda : controller.show_frame(StartPage))

        # putting the button in its place by
        # using grid
        button2.grid(row = 2, column = 1, padx = 10, pady = 10)


# Driver Code
app = tkinterApp()
app.mainloop()
