import tkinter.scrolledtext


from tkinter import *
import threading

class GuiMessage(threading.Thread):

    def __init__(self, client):
        self.client = client
        self.gui_loop()
        
    def gui_loop(self):
        self.win = tkinter.Tk()
        self.win.configure(bg="lightgray")

        self.chat_label = tkinter.Label(self.win, text="Chat:", bg="lightgray")
        self.chat_label.config(font=("Arial", 12))
        self.chat_label.pack(padx=20, pady=5)

        self.text_area = tkinter.scrolledtext.ScrolledText(self.win)
        self.text_area.pack(padx=20, pady=5)
        self.text_area.config(state='disabled')

        self.msg_label = tkinter.Label(self.win, text="Message:", bg="lightgray")
        self.msg_label.config(font=("Arial", 12))
        self.msg_label.pack(padx=20, pady=5)

        self.input_area = tkinter.Text(self.win, height=3)
        self.input_area.pack(padx=20, pady=5)

        self.send_button = tkinter.Button(self.win, text="Envoyer", command=self.write)
        self.send_button.config(font=("Arial", 12))
        self.send_button.pack(padx=20, pady=5)

        self.gui_done = True

        self.win.protocol("WM_DELETE_WINDOW", self.stop)

        self.win.mainloop()

    def write(self):
        #message = f"{self.username}: {self.input_area.get('1.0', 'end')}"
        #'{"romu": {"x": 0, "y": 0, "life": 3, "power": 1}, "JY": {"x": 13, "y": 13, "life": 3, "power": 1}, "timer": 246, "bombs": [], "lastMessageSend": {"from": "", "content": "{self.input_area.get('1.0', 'end')}"}}'
        message = f"{self.input_area.get('1.0', 'end')}"
        self.client.socket.send(message.encode('utf-8'))
        self.input_area.delete('1.0', 'end')

    def stop(self):
        self.running = False
        self.win.destroy()
        self.client.socket.close()
        exit(0)

    def receive(self):
        while self.running:
            try:
                message = self.client.socket.recv(59001)
                if message == 'NICK':
                    self.client.socket.send(self.username.encode('utf-8'))
                else:
                    if self.gui_done:
                        self.text_area.config(state='normal')
                        self.text_area.insert('end', message)
                        self.text_area.yview('end')
                        self.text_area.config(state='disabled')
            except ConnectionAbortedError:
                break
            except:
                print("Error")
                self.client.socket.close()
                break