import threading
from guiCon import Gui
from guiMessage import GuiMessage
from gameGui import Game
import argparse

class Main:

    whoami = None
    game_thread = None
    game = None

    def __init__(self, host=None, port=None, name=None):
        self.startGame = False
        self.data = ""
        self.Gui_con = Gui(self, host=host, port=port, name=name)
        self.client = self.Gui_con.get_client()
        self.Gui_message = GuiMessage(self.client)
        self.Gui_message.gui_loop().start()

    def start(self, whoami):
        print("main.start")
        self.whoami = whoami
        self.game = Game(main=self)
        print("game.run")
        self.game.start()
        print("main.begin")
        self.game_thread = threading.Thread(target=self.begin)
        self.game_thread.daemon = True
        self.game_thread.start()

    def callbackMsg(self, data=None):
        # here, handle different msg
        # print("mcm. received: ", data)
        self.data = data
        if self.game:
            self.game.handled = False

    def begin(self):
        oldData = self.data
        while(self.startGame):
            #On ne fait rien  sauf
            #Si on a recu un message
            if not self.game.handled:
                print("mb data is ", self.data)
                oldData = self.data
                if not self.data.startswith(self.whoami):
                    self.game.handle_adverse_move(self.data[3:])
                    self.game.handled = True

    def send(self, message):
        self.client.send(message)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server")
    parser.add_argument("-p", "--port")
    parser.add_argument("-n", "--name")
    args = parser.parse_args()
    host = args.server
    port = int(args.port)
    name = args.name
    Main(host, port, name)
