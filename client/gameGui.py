import threading
import socket
import time
import json
import re
import pygame
from pygame.locals import *
from datetime import datetime, timedelta
import tkinter
from tkinter import *

from constant import *
from classes import *
import threading


class Game(threading.Thread):

    whoami = None
    main = None
    perso = None
    adverse = None
    handled = False

    def __init__(self, main):
        super().__init__()
        # initialisation de pygame
        self.main = main
        print(main, self.main.startGame)
        pygame.display.init()
        self.whoami = main.whoami
        # initialisation de la fenetre
        self.fenetre = pygame.display.set_mode((cote_fenetre, cote_fenetre))
        pygame.display.set_caption(titre_fenetre)

    def run(self):
        # variable de debut/fin de la boucle infinie
        continuer = 1

        # boucle d'actualisation de la fenetre
        while continuer:
            # chargement de l'accueil
            accueil = pygame.image.load(image_accueil).convert()
            self.fenetre.blit(accueil, (0, 0))

            # rafraichissement
            pygame.display.flip()

            continuer_jeu = 1
            choix =1

            # Vérification du choix du niveau pour ne pas charger si il quitte ;)
            if choix != 0:
                # chargement du fond
                # fond = pygame.image.load(image_fond).convert()

                # generation du niveau à partir du fichier
                niveau = Niveau("level.txt")
                niveau.generer()
                niveau.afficher(self.fenetre)

                # création des deux avatars
                perso = Perso(p1_droite, p1_gauche, p1_haut, p1_bas, niveau)
                perso2 = Perso2(p2_droite, p2_gauche, p2_haut, p2_bas, niveau)
                if self.whoami == "P1":
                    self.perso = perso
                    self.adverse = perso2
                elif self.whoami == "P2":
                    self.perso = perso2
                    self.adverse = perso
                # création des deux bombes
                bombe = Bomb(image_bombe, niveau, perso)
                self.bombe2 = Bomb(image_bombe, niveau, perso2)
                # création des flammes
                flamme = Flammes(flamme_d, flamme_g, flamme_h, flamme_b)
            print("game.start")
            # boucle de jeu
            while continuer_jeu:
                # limitation de la vitesse de la boucle infinie
                pygame.time.Clock().tick(30)
                # print(pygame.time.get_ticks())

                # boucle des evenements de pygame
                for event in pygame.event.get():

                    # quitter le jeu [X][-][□]
                    if event.type == QUIT:
                        continuer_jeu = 0
                        continuer = 0

                    elif event.type == KEYDOWN:
                        # retour au menu
                        if event.key == K_ESCAPE:
                            continuer_jeu = 0
                        if event.key == K_SPACE:
                            bombe.poser(self.perso.x, self.perso.y, image_bombe)
                            self.main.send(f"{self.whoami} BOOM")
                        # touches de self.perso
                        if event.key == K_e:
                            bombe.poser(self.perso.x, self.perso.y, image_bombe)
                            self.main.send(f"{self.whoami} BOOM")
                        elif event.key == K_d:
                            self.perso.deplacer("droite")
                            self.main.send(f"{self.whoami} RIGHT")
                        elif event.key == K_q:
                            self.perso.deplacer("gauche")
                            self.main.send(f"{self.whoami} LEFT")
                        elif event.key == K_s:
                            self.perso.deplacer("bas")
                            self.main.send(f"{self.whoami} DOWN")
                        elif event.key == K_z:
                            self.perso.deplacer("haut")
                            self.main.send(f"{self.whoami} UP")

                # définition des affichages au nouvelles positions
        
                niveau.afficher(self.fenetre)
                self.fenetre.blit(self.perso.direction, (self.perso.x, self.perso.y))
                self.fenetre.blit(self.adverse.direction, (self.adverse.x, self.adverse.y))
                self.fenetre.blit(bombe.bomb, (bombe.x, bombe.y))
                self.fenetre.blit(self.bombe2.bomb, (self.bombe2.x, self.bombe2.y))

                # affichage des flammes de l'explosion (à simplifier avec la class flamme)
                # A integrer la classe flamme dans la class bombe pour simplifier
                if bombe.explosion == 1:
                    self.fenetre.blit(flamme.fflamme_b, (bombe.x, bombe.y + taille_sprite))
                    self.fenetre.blit(flamme.fflamme_h, (bombe.x, bombe.y - taille_sprite))
                    self.fenetre.blit(flamme.fflamme_g, (bombe.x - taille_sprite, bombe.y))
                    self.fenetre.blit(flamme.fflamme_d, (bombe.x + taille_sprite, bombe.y))

                if self.bombe2.explosion == 1:
                    self.fenetre.blit(flamme.fflamme_b, (self.bombe2.x, self.bombe2.y + taille_sprite))
                    self.fenetre.blit(flamme.fflamme_h, (self.bombe2.x, self.bombe2.y - taille_sprite))
                    self.fenetre.blit(flamme.fflamme_g, (self.bombe2.x - taille_sprite, self.bombe2.y))
                    self.fenetre.blit(flamme.fflamme_d, (self.bombe2.x + taille_sprite, self.bombe2.y))

                # affichage de la frame
                pygame.display.flip()

                # verification des conditions de victoire
                game_over = bombe.exploser()
                if game_over == 1:
                    continuer_jeu = 0
                    print("game over")
                game_over = self.bombe2.exploser()
                if game_over == 1:
                    continuer_jeu = 0
                    print("game over")

    def handle_adverse_move(self, move=None):
        if move == "UP":
            self.adverse.deplacer("haut")
        elif move == "DOWN":
            self.adverse.deplacer("bas")
        elif move == "LEFT":
            self.adverse.deplacer("gauche")
        elif move == "RIGHT":
            self.adverse.deplacer("droite")
        elif move == "BOOM":
            self.bombe2.poser(self.adverse.x, self.adverse.y, image_bombe)
