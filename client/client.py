import threading
import socket
import time
import json
from pygame.locals import *

from tkinter import *

from constant import *
from classes import *

__name__ = "bombermineroo"
__author__ = "La Communauté de l'Agneau"


class Client(threading.Thread):

    whoami = None
    main_thread = None
    def __init__(self, username, server, port, main):
        super().__init__()
        self.main_thread = main
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((server, port))
        self.username = username
        self.send("USERNAME {0}".format(username))
        self.listening = True
        self.start()

    def listener(self):
        while self.listening:
            data = ""
            try:
                data = self.socket.recv(59001).decode('UTF-8')
            except Exception as e:
                print(f"Unable to receive data: {repr(e)}")
            self.handle_msg(data)
            time.sleep(0.1)

    def listen(self):
        self.listen_thread = threading.Thread(target=self.listener)
        self.listen_thread.daemon = True
        self.listen_thread.start()

    def send(self, message):
        try:
            self.socket.sendall(message.encode("UTF-8"))
        except socket.error:
            print("unable to send message")

    def tidy_up(self):
        self.listening = False
        self.socket.close()

    def handle_msg(self, data=None):
        # EQUIVALENT DES ROUTES POUR ACTION REDONDANTE
        if data == "QUIT":
            self.tidy_up()
        # (genre si je t'envoie TOUCHE, tu considères que tu t'est fait toucher par une bombe) ;)
        elif data.startswith("START P") and not self.main_thread.startGame:
            whoami = data[-2:]
            print("DEMARRER LA PARTIIIIE")
            self.callbackStart(whoami)
        else:
            self.main_thread.callbackMsg(data)

    def callbackStart(self, whoami):
        self.whoami = whoami
        print("I'm", whoami)
        self.main_thread.startGame = True
        self.main_thread.start(whoami)


if __name__ == "__main__":
    username = input("username: ")
    server = "localhost"
    port = 59001
    client = Client(username, server, port)
    client.listen()
    message = ""
    while message != "QUIT":
        message = input()
        if(message=="SEND"):
            #DUMMY DATA 1
            message=json.dumps({
                "aze": {
                    "x": 15,
                    "y": 7,
                    "life": 3
                },"eza": {
                    "x": 18,
                    "y": 9,
                    "life": 3
                },"timer": 259,
                "bombs": [{
                    "x": 15,
                    "y": 7,
                    "throwAt": 259,
                    "power": 2
                },{
                    "x": 13,
                    "y": 7,
                    "throwAt": 259,
                    "power": 2
                }],"lastMessageSend": {"from":"eza",
                                       "content":"Connaw"}
            })
        elif(message=="BOMB"):
            #DUMMY DATA 2
            message=json.dumps({
                "aze": {
                    "x": 8,
                    "y": 7,
                    "life": 1
                },"eza": {
                    "x": 18,
                    "y": 9,
                    "life": 1
                },"timer": 259,
                "bombs": [{
                    "x": 7,
                    "y": 7,
                    "throwAt": 280,
                    "power":1
                }],"lastMessageSend": {"from":"aze",
                                       "content":"jE VaiS gAgné"}
            })
        client.send(message)
