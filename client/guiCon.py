from tkinter import *
import tkinter
from tkinter import simpledialog

from client import Client
import threading
from gameGui import Game

class Gui(threading.Thread):

    main_thread = None

    def __init__(self, main, host=None, port=None, name=None):
        self.main_thread = main
        self.msg = tkinter.Tk()
        self.msg.withdraw()
        self.username = simpledialog.askstring("Username", "Please choose choose a nickname") if not name else name
        self.server = simpledialog.askstring("Host", "host") if not host else host
        self.port = int(simpledialog.askstring("Port", "port")) if not port else port
        self.gui_done = False
        self.running = True

        self.client = Client(self.username, self.server, self.port, self.main_thread)
        self.client.listen()

    def get_client(self):
        return self.client

    
if __name__ == "__main__":
    Gui()