#!/bin/bash

session="WATCHER_S"
window="Win"

tmux kill-session -t "$session" 2>&1

#Get width and lenght size of terminal, this is needed if one wants to resize a detached session/window/pane
#with resize-pane command here
set -- $(stty size) #$1=rows, $2=columns

#start a new session in dettached mode with resizable panes
tmux new-session -s $session -n $window -d -x "$2" -y "$(($1 - 1))"
tmux send-keys -t $session 'python3 server/server.py' C-m

#rename pane 0 with value of $pan1
tmux set -p @mytitle "server"

#split window vertically
tmux split-window -v
tmux send-keys -t $session 'cd client/' C-m
tmux send-keys -t $session 'python3 ./main.py -n p1 -s localhost -p 59001' C-m
tmux set -p @mytitle "cli 1"

tmux split-window -h
tmux send-keys -t $session 'cd client/' C-m
tmux send-keys -t $session 'python3 ./main.py -n p2 -s localhost -p 59001' C-m
tmux set -p @mytitle "Cli 2"

tmux attach -t $session