# coding=utf-8
from time import time 
import json 
import threading 

from classes import Player, Bomb, LastMesageSend

class Game(threading.Thread):
    def __init__(self, server):
        super().__init__()
        #self.schema={"player1": {"x": 0,"y": 0},"player2": {"x": 12,"y": 12},"bombs": [],"lastMessageSend": {}}
        self.startAt=1 #On récupère le timestamp
        self.endAt=1 #On set la fin de partie a 5mn (300s)
        self.timer=300 #
        self.server= server
        self.lastMessageSend=LastMesageSend()
        self.schema= {}
        self.data="{}"
        self.bombs=[]
        #VALUE OK ?
        self.server.echo("INIT")
        
    def setP1(self, player):
        self.player1=player
        
    def getP1(self):
        return self.player1
        
    def setP2(self, player):
        self.player2=player
        
    def getP2(self):
        return self.player2
    
    def setSchema(self):
        self.schema = json.dumps({
            self.player1.name: {
                "x": self.player1.x,
                "y": self.player1.y ,
                "life": self.player1.life,
                "power":self.player1.power
            },
            self.player2.name: {
                "x": self.player2.x,
                "y": self.player2.y ,
                "life": self.player2.life,
                "power":self.player1.power
            },
            "timer":self.timer,
            "bombs":self.bombs,
            "lastMessageSend":
                self.lastMessageSend.toJson()
        })

    #Callback from clientThread, get player MSG here
    def retrieveMsg(self, data):
        self.data = data

    #Callback from clientThread, get instantiate players here
    def retrieveUname(self, username,id):
        if(id==0):  
            self.player1=Player(username,id)
        else:
            self.player2=Player(username,id)
        
        
        #Cette méthode est appelée afin d'actualiser les données envoyé par les clients
    def updateGame(self):
        print(self.data)
        jsonGame=json.loads(self.data)

        #on envoie le json du p1 pour update p1 en local
        self.player1.updatePlayer(jsonGame[self.player1.name])
        
        #on envoie le json du p2 pour update p2 en local
        self.player2.updatePlayer(jsonGame[self.player2.name])
        
        if(self.lastMessageSend.HasChanged(jsonGame['lastMessageSend'])):
            self.lastMessageSend.updateMessage(jsonGame['lastMessageSend'])
        for entry in jsonGame['bombs']:
            bomb=Bomb(entry)
            print(bomb)
            status=bomb.updateBomb(self.timer)
            print(status)
            
            if(isinstance(status, list)):
                #La bombe a exploser
                for case in status:
                    if(case['x']==self.player1.x and case['y']==self.player1.y):
                        #Joueur 1 touché
                        self.player1.touch()
                        print("player1 touch")
                    if(case['x']==self.player2.x and case['y']==self.player2.y):
                        self.player2.touch()
                        print("player2 touch")
        
    def begin(self):
        self.startAt = int(time())
        self.endAt = self.startAt+300
        oldData=self.data
        while(self.timer >0):
            
            if(oldData!=self.data):
                self.updateGame() #mise à jour des données reçu
                self.setSchema() #mise à jour du schéma à renvoyer
                self.server.echo(self.schema) #Renvoie du schema a jour
                oldData = self.data
            
            #UPDATE DE TIMER
            if(self.timer!=self.endAt-int(time())):
                self.timer=self.endAt-int(time())
                print(self.timer)
                self.setSchema()
                self.server.echo(self.schema)
            
        #Fin de partie                
        self.server.echo("TIMEOUT")
                
    def end(self, message):
        self = None

