# coding=utf-8
import json
from time import time


class Player():
    def __init__(self, name, id):
        self.name = name
        if(id == 0):
            self.x = 0
            self.y = 0
        else:
            self.x = 13
            self.y = 13
        self.life = 3
        self.name = name
        self.id = id
        self.power = 1

    def updatePlayer(self, player):
        self.move(player['x'], player['y'])  # update position
        return self.toJson()

    def move(self, x, y):
        # if(0<i>14)
        self.x = x
        self.y = y

    def touch(self):
        self.life -= 1
        if(self.life == 0):
            self.server.echo(self.name+" DIE")

    def toJson(self):
        jsonRes = {
            "x": self.x,
            "y": self.y,
            "life": self.life,
            "power": self.power
            }
        return json.dumps(jsonRes)


class Bomb():
    TTL = 2  # Time To Live before Boom

    def __init__(self, x, y, throwAt, power):
        self.x = x
        self.y = y
        self.throwAt = throwAt
        self.power = power
        
    def __init__(self, bomb):
        self.x = bomb['x']
        self.y = bomb['y']
        self.throwAt = bomb['throwAt']
        self.power = bomb['power']

    def updateBomb(self, timer):
        if(self.throwAt+self.TTL < timer):
            print("boom")
            return self.boom() # retourne un array de case touchée par la bombe
        else:
            return self.toJson()

    def toJson(self):
        jsonRes = {
            "x": self.x,
            "y": self.y,
            "throwAt": self.throwAt,
            "power": self.power
            }
        return json.dumps(jsonRes)

    # retourne un array de case prise dans la deflagration
    def boom(self):
        case = []
        print(self.power)
        pos = {
                "x": self.x,
                "y": self.y
            }
        case.append(pos)
        for i in range(1, self.power+1):
            print(self.power)
            print(i)
            revertX = {
                "x": (self.x-i),
                "y": (self.y)
            }
            case.append(revertX)
            revertY = {
                "x": (self.x),
                "y": (self.y-i)
            }
            case.append(revertY)
            normalX = {
                "x": (self.x+i),
                "y": (self.y)
            }
            case.append(normalX)
            normalY = {
                "x": (self.x),
                "y": (self.y+i)
            }
            case.append(normalY)
        return case

class LastMesageSend():
    
    def __init__(self):
        self.From=""
        self.content=""
    
    def updateMessage(self, message):
        self.From=message['from']
        self.content=message['content']
    
    def HasChanged(self, message):
        if(message['from']!= self.From or message['content']!=self.content):
            return True
        else:
            return False
        
    def toJson(self):
        jsonRes={
            "from":self.From,
            "content":self.content
        }
        return jsonRes
