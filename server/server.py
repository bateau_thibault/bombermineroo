# coding=utf-8
import socket
import signal
import sys # utilisé pour sortir du programme
import time
from clientThread import ClientListener
from game import Game

class Server():

    def __init__(self, port):
        self.listener= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listener.bind(('', port))
        self.listener.listen(1)
        self.clients_sockets = []
        self.player_threads = []
        self.players = []
        self.gameThread=None
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)
        print("Listening on port", port)
        
    def signal_handler(self, signal, frame):
        self.listener.close()
        self.echo("QUIT")
        
    def run(self):
        #server mainLoop
        run=True
        while run:
            #init new game
            self.gameThread=Game(server=self)
            i=0 #Player id "securise" connection
            while i<2:
                print("Wait for Player "+str(i))
                try:
                    (client_socket, client_adress) = self.listener.accept()
                except socket.error:
                    sys.exit("Cannot connect Player "+str(i))
                self.clients_sockets.append((client_socket, i+1))
                client_thread=ClientListener(self, client_socket, client_adress , i ,self.gameThread.retrieveMsg, self.gameThread.retrieveUname) #init Client listener in new thread
                client_thread.start()
                time.sleep(0.1)
                i+=1

            #no other connection are technically possible
            time.sleep(1)#pause before start

            # SEND "START" SO THE CLIENT KNOW THAT TWO PLAYER HAVE JOIN
            for j in range(0, i):
                j+=1
                print(f"START P{j}")
                self.echo(f"START P{j}", target=j)
            # THAT MEAN, GAME CAN BEGIN
            self.gameThread.start() #start game in thread
            self.gameThread.begin()
            print("HEY")
            #HERE, WE CALL OUR GUI

    def remove_socket(self, socket):
        self.client_sockets.remove(socket)

    def echo(self, data, target=None):
        for sock in self.clients_sockets:
            if (target and sock[1] == target) or not target:
                try:
                    print(data)
                    sock[0].sendall(data.encode("UTF-8"))
                except Exception as e:
                    print(f"Cannot send the message : {repr(e)}")

#run server
server=Server(59001)
server.run()