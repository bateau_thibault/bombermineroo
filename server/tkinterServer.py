
# coding=utf-8from tkinter import *
import _thread
from server import Server



class serverGUI:
    
    def __init__(self ):
        self.root = Tk()
        self.root.title('SERVER GUI') #define GUI title
        self.root.geometry('400x200') #GUI size
        self.lbl = Label(self.root, text = "Renseigner le port :").pack()
        #Input (port)
        self.portText = Text(self.root, height=1, width=5 )
        self.portText.pack()
        # Button Creation 
        self.startButton = Button(self.root, text = "START", command = self.startServerWithPort)
        self.startButton.pack()
        print("hello")
        self.quitButton = Button(self.root, text = "QUIT", command = self.root.destroy).pack()

        _thread.start_new_thread(self.root.mainloop(), ())

    def startServerWithPort(self):
        inp = self.portText.get(1.0, "end-1c") #GET LABEL INPUT
        srv=Server(int(inp))  #SET SERVER PORT
        srv.echo("Listening on port : " + inp)
        _thread.start_new_thread(srv.run())    #START SERVER
        
if __name__=="__main__":
    serverGUI()