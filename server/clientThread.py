# coding=utf-8
from socket import socket
import threading 
import re 
import time
from game import Player

class ClientListener(threading.Thread):

    def __init__(self, server, socket, address, id, callbackMsg, callbackUname):
        super(ClientListener, self).__init__()
        self.server= server
        self.socket=socket
        self.address= address
        self.listening= True
        self.username= None
        self.id=id
        self.callbackMsg=callbackMsg
        self.callbackUname=callbackUname
        
    def run(self):
        while self.listening:
            data= ""
            try:
                data = self.socket.recv(1024).decode('UTF-8')
            except socket.error:
                print("Unable to receive data")
            self.username = re.search('^USERNAME (.*)$', data)
            self.handle_msg(data)
            time.sleep(0.1)
        print("Ending client thread for", self.address)

    def quit(self):
        self.listening = False
        self.socket.close()
        self.server.remove_socket(self.socket)
        self.server.echo("{0} has quit\n".format(self.username))
        
        
    def handle_msg(self, data):
        username_result = re.search('^USERNAME (.*)$', data)
        if username_result:
            #New player in canal !!
            self.username = username_result.group(1)
            self.server.echo("{0} has joined.\n".format(self.username))
            self.callbackUname(self.username, self.id)
            #Let's declare him :)
            #
        elif data == "QUIT":
            self.quit()
        elif data == "":
            self.quit()
        else:
            #response = self.game.update(data)
            self.callbackMsg(data)
            self.server.echo(data)
            
    def getUName(self):
        return self.username
            
    def getPlayer(self):
        return self.player